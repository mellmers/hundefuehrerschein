import { createApp } from 'vue'
import { createStore } from 'vuex'
import { loadState, saveState } from '../localStorage'
import createVuetify from './plugins/vuetify'
import App from './App.vue'
import router from './router'

/** Styles **/
import './assets/styles/app.scss'

// Create a new store instance.
const intitialState = {
    currentQuestion: 1,
    questionsCount: 0,
};
const persistedState = loadState();
const store = createStore({
    state () {
        return {...intitialState, ...persistedState}
    },
    mutations: {
        setCurrentQuestion (state, currentQuestion) {
            state.currentQuestion = currentQuestion
        },
        setQuestionsCount (state, questionsCount) {
            state.questionsCount = questionsCount
        }
    }
})
store.subscribe((mutation, state) => {
    saveState(state)
})

const app = createApp(App)
const vuetify = createVuetify

app.use(store)
app.use(router)
app.use(vuetify)

app.mount('#app')
