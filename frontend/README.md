# Hundeführerschein Lern-App

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Capacitor Workflow

### Build app
```
npm run build
```

### Sync your Project
You may wish to sync your web app with your native project(s) in the following circumstances:

* When you want to copy web assets into your native project(s).
* Before you run your project using a Native IDE.
* After you install a new Capacitor plugin.
* After you clone your project.
* When you want to setup or reconfigure the native project(s) for Capacitor.
* When you want to install native dependencies (e.g. with Gradle or CocoaPods).

To sync your project, run:
```
npx cap sync
```

### Run your Project
```
npx cap run (android | ios)
```

### Updating Capacitor
To update Capacitor Core and CLI:
```
npm install @capacitor/cli
npm install @capacitor/core
```
To update any or all of the platforms you are using:
```
npm install @capacitor/ios
npm install @capacitor/android
```

[Click here for more information](https://capacitorjs.com/docs/basics/workflow)
